import React from 'react';
import './App.css';
import NetworkGraph from './network/NetworkGraph';

function App() {
  return (
    <div className="App">
      <NetworkGraph></NetworkGraph>
    </div>
  );
}

export default App;
