import React from 'react';
import vis from 'vis';
import './style.css';

class NetworkGraph extends React.Component {
    constructor(props) {
        super(props);

        this.graphOptions = {
            nodes: {
                shape: "dot",
                size: 16
            },
            manipulation: {
                editNode: (data, callback) => { }
            },
            // responsible of graph deploy animation and placement
            physics: {
                forceAtlas2Based: {
                    gravitationalConstant: -26,
                    centralGravity: 0.005,
                    springLength: 230,
                    springConstant: 0.18
                },
                maxVelocity: 146,
                solver: "forceAtlas2Based",
                timestep: 0.35,
                stabilization: { iterations: 150 }
            }
        }

        this.networkGraphRef = React.createRef();

        this.state = {
            network: undefined,
            data: undefined
        }
    }

    componentDidMount = () => this.load()

    shouldComponentUpdate = () => false

    load = async () => {
        var edges = [
            { from: 1, to: 3 },
            { from: 1, to: 2 },
            { from: 2, to: 4 },
            { from: 2, to: 5 },
            { from: 3, to: 3 }
        ];
        var nodes = [
            { id: 1, label: 'Node 1' },
            { id: 2, label: 'Node 2' },
            { id: 3, label: 'Node 3' },
            { id: 4, label: 'Node 4' },
            { id: 5, label: 'Node 5' }
        ];
        nodes = new vis.DataSet(nodes);
        edges = new vis.DataSet(edges);

        var data = {
            nodes,
            edges
        }

        this.setState(state => {
            let network = new vis.Network(this.networkGraphRef.current, data, this.graphOptions);
            this.loadGraphEvents(network);
            return {
                network,
                data
            };
        })

        let timer = setInterval(() => {
            let nodes = this.state.data.nodes;
            let rand = Math.floor(Math.random() * nodes.length);

            this.addNode(rand, Math.floor(Math.random() * 10));
        }, 50);
        setTimeout(() => clearInterval(timer), 50000);
    }

    addNode = (parent, count = 1) => {
        this.setState(state => {

            const node = state.network.getPositions([parent])[parent];

            let newNodes = [];
            for (let i = 0; i < count; i++) {
                let n = {
                    label: 'new' + (state.data.nodes.length + 1 + i),
                    id: state.data.nodes.length + 1 + i
                }

                if (node !== undefined) {
                    n.x = node.x;
                    n.y = node.y;
                }

                newNodes.push(n);
            }


            let ids = state.data.nodes.add(newNodes)
            let newEdges = ids.map(id => ({
                from: parent,
                to: id
            }))
            state.data.edges.add(newEdges)

            return state;
        })
    }

    loadGraphEvents = (networkGraph) => {
        networkGraph.on('click', (params) => {
            if ((params.nodes.length == 1)) {
                //this.addNode(params.nodes[0]);
            }
        })
    }

    render() {
        return (<div className="network-graph" >
            <div className="network" ref={this.networkGraphRef} > Network Grpah </div>
        </div>
        )
    }
}

export default NetworkGraph;